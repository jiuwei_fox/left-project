import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/pages/home.vue'
// import ShopList from '@/pages/shoplist.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/shoplist',
    name: 'ShopList',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../pages/shoplist.vue')
  },
  {
    path: '/photoinfo',
    name: 'PhotoInfo',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../components/PhotoInfo.vue')
  },
  {
    path: '/morephoto',
    name: 'MorePhoto',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../components/MorePhoto.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
