import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.config.productionTip = false
//使用axios
Vue.prototype.$axios=axios
Vue.use(MintUI)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
